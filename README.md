# avocado_blog

Exercice pratique du cours 3

### Installation

Avant tous, il vous faudra installer les librairies dont aura besoin cette application web pour fonctionner.

Pour ce faire, executer la commande suivante :

```
npm install
```

### Lancer l'application

Pour lancer l'application vous devrez utiliser la commande suivante :

```
npm run serve
```

Puis, vous rendre à l'adresse suivante :

```
http:/localhost:8080
```

### Struture de l'application

```
main.js--App.vue---|---Header.vue
                   |
                   |---Main.vue---Content.vue---|---ContentHeader.vue
                   |                            | 
                   |                            |---ContentArticle.vue
                   |                            |
                   |                            |---ContentComment.vue---Comment.vue
                   |
                   |---Footer.vue
```